import LoginForm from './LoginForm.vue'
import MobileForm from './MobileForm.vue'
import LoginFormTitle from './LoginFormTitle.vue'
import RegisterForm from './RegisterForm.vue'
import QrCodeForm from './QrCodeForm.vue'
import SSOLoginVue from './SSOLogin.vue'
import PerfectForm from './PerfectForm.vue'
import IdentityForm from './IdentityForm.vue'
import IdentityFormc from './IdentityFormc.vue'
import IdentitySel from './IdentitySel.vue'

export { LoginForm, MobileForm, LoginFormTitle, RegisterForm, QrCodeForm, SSOLoginVue, PerfectForm, IdentityForm , IdentityFormc, IdentitySel}

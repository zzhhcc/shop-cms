import { store } from '@/store'
import { defineStore } from 'pinia'

export const useRegisterStore = defineStore('registerState', {
  state: () => ({
    registerType: '0', // register or login
  }),
  getters: {
    getRegisterType(): string {
      return this.registerType
    },
  },
  actions: {
    setRegisterType(registerType: string) {
      this.registerType = registerType
    },
  },
})

export const useRegisterStores = () => {
  return useRegisterStore(store)
}